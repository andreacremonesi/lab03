import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Random;

public class MainGame{
	public static void main(String [] args){
		Logger logger = LoggerFactory.getLogger(MainGame.class);
    		logger.info("Welcome to svigroup bowling!\n");

		Random randomObject=new Random();
		Game match=new Game();
		int shoot;
		for(int frame_index=0;frame_index<10;frame_index++){
			int shoot_counter=0;
			shoot=0;
			logger.info(frame_index+1+"° Frame ");

			
				
				while(shoot_counter<2){
					int randomSpace=10-shoot+1;
					shoot=randomObject.nextInt(randomSpace);
					match.roll(shoot);
					if(shoot==10){
						shoot_counter+=2;
						if(frame_index<9)
							logger.info("X \n");
						else	logger.info("X ");
					}
					else{
						logger.info(shoot+" ");
						shoot_counter++;
						if (shoot_counter==1 && match.getSpareFlag()){
							if(frame_index<9)
								logger.info("/ \n");
							else 	logger.info("/ ");
							if(!match.getSpareFlag())
								logger.info(shoot+" \n");
						}
					}
				}
				if (frame_index==9 && (match.getSpareFlag() || match.getStrikeFlag())){
					if(match.getSpareFlag()){
						shoot=randomObject.nextInt(11);
						match.roll(shoot);
						if(shoot==10) logger.info("X \n");
						else	logger.info(shoot+" \n");
					}	
					if(match.getStrikeFlag()){
						shoot=randomObject.nextInt(11);
						match.roll(shoot);
						if(shoot==10) logger.info("X \n");
						else	logger.info(shoot+" \n");
			
						shoot=randomObject.nextInt(10-shoot+1);
						match.roll(shoot);
						if(shoot==10) logger.info("X \n");
						else	logger.info(shoot+" \n");
					}				
			
				}
			logger.info("Score at frame "+ frame_index +" : "+ match.score());
		}
		
	}
}
