public class Game{
	private int score;
	private int previous;
	private boolean first_flag=true;
	private boolean spare_flag=false;
	private boolean strike_flag=false;
	private boolean strike_row=false;
	public Game(){
		score=0;
		previous=0;
	}
	public int score(){
		return score;
	}
	public void roll(int pins){
		if(first_flag){
			if(!strike_flag && !spare_flag){
				if(pins==10){
					strike_flag=true;
				}
				else{
					previous=pins;

					first_flag=false;
				}
			}
			else{
				if(strike_flag){
					if(pins==10){
						if(strike_row)
							score+=30;
			
					strike_flag=true;
					strike_row=true;
					}
					else{	
						if(strike_row)
							score+=20+pins;

						previous=pins;

						first_flag=false;
		
					}
				}else{
					if(spare_flag){
						if(pins==10){
							strike_flag=true;
			
						}
						else{
							previous=pins;

							first_flag=false;
						}
						score+=10+pins;
					}
				}
			}
			spare_flag=false;
		}
		else{
			if(strike_flag){
				score+=10+previous+pins;
			}

			if(previous+pins==10){
				spare_flag=true;
			}else{
				score+=previous+pins;
			}

			
			
			first_flag=true;
			strike_flag=false;
		}
	}
	public boolean getStrikeFlag(){
		return strike_flag;
	}	
	public boolean getSpareFlag(){
		return spare_flag;
	}
	public boolean getStrikeRow(){
		return strike_row;
	}
}
